function validate(){
    var letters = /^[a-zA-Z]+$/,num = /^[0-9]+$/,usrstr=/^[a-zA-Z0-9]+$/,spchar,pass,cnfPsw;
    spchar=/^[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*$/;
    
    namecheck=names(document.getElementById("firstname").value,document.getElementById("lastname").value,letters);
        gen=gender();
        lanCheck=lang(document.getElementById("languages").value);
        phnCheck=phone(document.getElementById("phonenumber").value,num);
        emailCheck=email_id(document.getElementById("email").value);
        userCheck=username(document.getElementById("usrname").value,usrstr);
        passLenCheck=passlength(document.getElementById("psw").value);
        passMetch=passmetch(document.getElementById("psw").value,document.getElementById("cnfpsw").value);
    if(namecheck&&gen&&lanCheck&&phnCheck&&emailCheck&&userCheck&&passMetch){
        return true;

    }
    else{
        namecheck=names(document.getElementById("firstname").value,document.getElementById("lastname").value,letters);
        gen=gender();
        lanCheck=lang(document.getElementById("languages").value);
        phnCheck=phone(document.getElementById("phonenumber").value,num);
        emailCheck=email_id(document.getElementById("email").value);
        userCheck=username(document.getElementById("usrname").value,usrstr);
        passLenCheck=passlength(document.getElementById("psw").value);
        passMetch=passmetch(document.getElementById("psw").value,document.getElementById("cnfpsw").value);
        return false;
        
    }
}
function names(firstname,lastname,letters){
    if(!firstname.match(letters)||!lastname.match(letters)){
        document.getElementById("fname").innerHTML="*Please enter correct value";
        document.getElementById("lname").innerHTML="*Please enter correct value";
        return false;
    }
    return true;
}username(document.getElementById("usrname").value);
function gender(){
    if(!document.getElementById("male").checked&&!document.getElementById("female").checked&&!document.getElementById("other").checked){
        document.getElementById("gname").innerHTML="*Please Select any one";
        return false;
    }
    return true;
}
function lang(languages){
    if(languages=="non"){
        document.getElementById("lang").innerHTML="* Select a Language";
        return false;
    }
    return true;

}
function phone(phonenumber,num){
    if(!phonenumber.match(num)||phonenumber.length<10){
        document.getElementById("phn").innerHTML="*Enter a valid Phone number";
        return false;
    }
    return true;
}
function email_id(emails){
    if(emails.search("@")==-1){
        document.getElementById("email-error").innerHTML="*Enter a valid email id";
        return false;
    }
    return true;
}
function username(user,usrstr){
    if(!user.match(usrstr))
    {
        document.getElementById("usr").innerHTML="*Enter a valid userid..";
        return false;
    }
    return true;
}
function passlength(password){
    if(password.length<6){
        document.getElementById("strong").innerHTML="* Length must be greater then 6";
        return false;
    }
    return true;
}
function passmetch(password,confpass){
    if(password!=confpass||confpass.length<6||password.length<6){
        document.getElementById("match").innerHTML="*Does not match Please try again latter";
        return false;
    }
    return true;
}

function reg_successfull(){
    if(validate()){
        document.getElementById("success").style.visibility="visible";
        alert("Registered Successfuly Now Submit Form");
    }
    else
    {
        document.getElementById("success").style.visibility="hidden";
        alert("Correct The High Lighted Fields");
    }

}
